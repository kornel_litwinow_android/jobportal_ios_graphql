//
//  ViewController.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 12/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import UIKit
import Apollo
import SDWebImage

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var listOffersJob = [GetJobsQuery.Data.Job]()
        
    enum ListSection: Int, CaseIterable {
       case offersJob
     }
     
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 65.0, right: 0.0)
        self.loadOffersJob()
    }
    
    private func loadOffersJob() {
         Network.shared.apollo
         .fetch(query: GetJobsQuery()) { [weak self] result in
          guard let data = try? result.get().data else {return}
            
          defer {
            self!.tableView.reloadData()
          }
                   
             switch result {
             case .success(let graphQLResult):
               if let jobConnection = graphQLResult.data?.jobs{
                   self!.listOffersJob.append(contentsOf: jobConnection.compactMap { $0 })
               }
                       
               if let errors = graphQLResult.errors {
                 let message = errors
                       .map { $0.localizedDescription }
                       .joined(separator: "\n")
                   self!.showErrorAlert(title: "GraphQL Error(s)", message: message)
               }
                
             case .failure(let error):
               self!.showErrorAlert(title: "Network Error", message: error.localizedDescription)
               }
             }
           }
       
       private func showErrorAlert(title: String, message: String) {
         let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "OK", style: .default))
         self.present(alert, animated: true)
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let selectedIndexPath = self.tableView.indexPathForSelectedRow else {return}
        
        if segue.identifier == "detailJobVC" {
                   if let destinationVC = segue.destination as? DetailJobVC {
                    let job = self.listOffersJob[selectedIndexPath.row]
                    destinationVC.applyUrl = job.applyUrl!
                    destinationVC.companyName = job.company!.name
                    destinationVC.cityName = job.cities!.compactMap{$0.name}.joined(separator: ",")
                    destinationVC.countryName = job.countries!.compactMap {$0.name}.joined(separator: "")
                    destinationVC.tagsName = job.tags!.compactMap{$0.name}.joined(separator: ",")
                    destinationVC.descriptionName = job.description!
                    destinationVC.titleJob = job.title
                    
                    if job.company?.logoUrl != nil {
                    destinationVC.logoUrl = (job.company?.logoUrl)!
                    }

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    let dateFormatterToShow = DateFormatter()
                    dateFormatterToShow.dateFormat = "yyyy-MM-dd"
                    let jobDataInString = job.postedAt
                    if let jobDataInDate = dateFormatter.date(from: jobDataInString) {
                        let jobDataToShow = dateFormatterToShow.string(from: jobDataInDate)
                        destinationVC.postedJob = jobDataToShow
                      } else {
                        print("There was an error decoding the string")
                            }
                         }
                       }
                     }
                 }

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      guard let listSection = ListSection(rawValue: section) else {
        assertionFailure("Invalid")
        return 0
      }
            
      switch listSection {
      case .offersJob:
        return self.listOffersJob.count
      }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OfferJobCell
        cell.titleLbl?.text = nil
        cell.companyLbl?.text = nil
        cell.locationLbl?.text = nil
          guard let listSection = ListSection(rawValue: indexPath.section) else {
            assertionFailure("Invalid section")
            return cell
          }
            
          switch listSection {
          case .offersJob:
            let city = self.listOffersJob[indexPath.row]
            cell.locationLbl.text = city.countries?.compactMap {$0.name}.joined(separator: "")
            cell.companyLbl.text = city.company?.name
            cell.titleLbl.text = city.title
            
            let placeholder = UIImage(named: "companyPlaceholder")!
            let logo = city.company?.logoUrl
            if logo == nil {
                cell.logoImg?.image = placeholder
                     } else {
                cell.logoImg?.sd_setImage(with: URL(string: logo!), placeholderImage: placeholder)
                }
              }
             return cell
         }

}
