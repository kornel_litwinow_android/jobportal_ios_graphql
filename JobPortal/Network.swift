//
//  Network.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 12/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import Foundation
import Apollo

class Network {
  static let shared = Network()
    
  private(set) lazy var apollo = ApolloClient(url: URL(string: "https://api.graphql.jobs/")!)
}
