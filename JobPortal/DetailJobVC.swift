//
//  DetailJobVC.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 13/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import UIKit
import SDWebImage

class DetailJobVC: UIViewController {
    
    @IBOutlet weak var companyImg: UIImageView!
    @IBOutlet weak var companyNameLbl: UILabel!
    @IBOutlet weak var localizationLbl: UILabel!
    @IBOutlet weak var dscTxtView: UITextView!
    @IBOutlet weak var techStackLbl: UILabel!
    @IBOutlet weak var jobTitleNavigationItem: UINavigationItem!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var applyBtn: UIButton!
    
    var titleJob = ""
    var logoUrl = ""
    var companyName = ""
    var cityName = ""
    var countryName = ""
    var tagsName = ""
    var descriptionName = ""
    var postedJob = ""
    var applyUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyBtn.layer.cornerRadius = 20.0
        companyNameLbl.text = companyName
        techStackLbl.text = tagsName
        
        if countryName != "" && cityName != "" {
        localizationLbl.text = countryName + "," + cityName
        } else if countryName == "" && cityName != "" {
            localizationLbl.text = cityName
        } else if countryName != "" && cityName == "" {
            localizationLbl.text = countryName
        } else {
            localizationLbl.text = "No location provided"
        }
        
        if description == "" {
            dscTxtView.text = "No job details"
        } else {
            dscTxtView.text = descriptionName
        }
        
        date.text = postedJob
        jobTitleNavigationItem.title = titleJob

        let placeholder = UIImage(named: "companyPlaceholder")!
        if  logoUrl != "" {
            companyImg.sd_setImage(with: URL(string: logoUrl), placeholderImage: placeholder)
            } else {
              companyImg.image = placeholder
         }
      }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebView" {
                       if let destinationVC = segue.destination as? WebViewVC {
                        destinationVC.url = applyUrl
          }
        }
      }
    }

extension UIViewController {
    open override func awakeFromNib() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    

}

