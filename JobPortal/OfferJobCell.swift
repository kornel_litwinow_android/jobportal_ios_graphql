//
//  OfferJobCell.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 13/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import UIKit
import SDWebImage

class OfferJobCell: UITableViewCell {
    
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
}
