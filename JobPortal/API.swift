//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class GetJobsQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    query getJobs {
      jobs {
        __typename
        id
        title
        description
        postedAt
        applyUrl
        cities {
          __typename
          id
          name
        }
        countries {
          __typename
          id
          name
        }
        remotes {
          __typename
          name
        }
        company {
          __typename
          id
          name
          logoUrl
          websiteUrl
        }
        tags {
          __typename
          name
        }
      }
    }
    """

  public let operationName = "getJobs"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("jobs", type: .nonNull(.list(.nonNull(.object(Job.selections))))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(jobs: [Job]) {
      self.init(unsafeResultMap: ["__typename": "Query", "jobs": jobs.map { (value: Job) -> ResultMap in value.resultMap }])
    }

    public var jobs: [Job] {
      get {
        return (resultMap["jobs"] as! [ResultMap]).map { (value: ResultMap) -> Job in Job(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: Job) -> ResultMap in value.resultMap }, forKey: "jobs")
      }
    }

    public struct Job: GraphQLSelectionSet {
      public static let possibleTypes = ["Job"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("postedAt", type: .nonNull(.scalar(String.self))),
        GraphQLField("applyUrl", type: .scalar(String.self)),
        GraphQLField("cities", type: .list(.nonNull(.object(City.selections)))),
        GraphQLField("countries", type: .list(.nonNull(.object(Country.selections)))),
        GraphQLField("remotes", type: .list(.nonNull(.object(Remote.selections)))),
        GraphQLField("company", type: .object(Company.selections)),
        GraphQLField("tags", type: .list(.nonNull(.object(Tag.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, title: String, description: String? = nil, postedAt: String, applyUrl: String? = nil, cities: [City]? = nil, countries: [Country]? = nil, remotes: [Remote]? = nil, company: Company? = nil, tags: [Tag]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Job", "id": id, "title": title, "description": description, "postedAt": postedAt, "applyUrl": applyUrl, "cities": cities.flatMap { (value: [City]) -> [ResultMap] in value.map { (value: City) -> ResultMap in value.resultMap } }, "countries": countries.flatMap { (value: [Country]) -> [ResultMap] in value.map { (value: Country) -> ResultMap in value.resultMap } }, "remotes": remotes.flatMap { (value: [Remote]) -> [ResultMap] in value.map { (value: Remote) -> ResultMap in value.resultMap } }, "company": company.flatMap { (value: Company) -> ResultMap in value.resultMap }, "tags": tags.flatMap { (value: [Tag]) -> [ResultMap] in value.map { (value: Tag) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return resultMap["title"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var postedAt: String {
        get {
          return resultMap["postedAt"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "postedAt")
        }
      }

      public var applyUrl: String? {
        get {
          return resultMap["applyUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "applyUrl")
        }
      }

      public var cities: [City]? {
        get {
          return (resultMap["cities"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [City] in value.map { (value: ResultMap) -> City in City(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [City]) -> [ResultMap] in value.map { (value: City) -> ResultMap in value.resultMap } }, forKey: "cities")
        }
      }

      public var countries: [Country]? {
        get {
          return (resultMap["countries"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Country] in value.map { (value: ResultMap) -> Country in Country(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Country]) -> [ResultMap] in value.map { (value: Country) -> ResultMap in value.resultMap } }, forKey: "countries")
        }
      }

      public var remotes: [Remote]? {
        get {
          return (resultMap["remotes"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Remote] in value.map { (value: ResultMap) -> Remote in Remote(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Remote]) -> [ResultMap] in value.map { (value: Remote) -> ResultMap in value.resultMap } }, forKey: "remotes")
        }
      }

      public var company: Company? {
        get {
          return (resultMap["company"] as? ResultMap).flatMap { Company(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "company")
        }
      }

      public var tags: [Tag]? {
        get {
          return (resultMap["tags"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Tag] in value.map { (value: ResultMap) -> Tag in Tag(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Tag]) -> [ResultMap] in value.map { (value: Tag) -> ResultMap in value.resultMap } }, forKey: "tags")
        }
      }

      public struct City: GraphQLSelectionSet {
        public static let possibleTypes = ["City"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, name: String) {
          self.init(unsafeResultMap: ["__typename": "City", "id": id, "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }

      public struct Country: GraphQLSelectionSet {
        public static let possibleTypes = ["Country"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, name: String) {
          self.init(unsafeResultMap: ["__typename": "Country", "id": id, "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }

      public struct Remote: GraphQLSelectionSet {
        public static let possibleTypes = ["Remote"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String) {
          self.init(unsafeResultMap: ["__typename": "Remote", "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }

      public struct Company: GraphQLSelectionSet {
        public static let possibleTypes = ["Company"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("logoUrl", type: .scalar(String.self)),
          GraphQLField("websiteUrl", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, name: String, logoUrl: String? = nil, websiteUrl: String) {
          self.init(unsafeResultMap: ["__typename": "Company", "id": id, "name": name, "logoUrl": logoUrl, "websiteUrl": websiteUrl])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var logoUrl: String? {
          get {
            return resultMap["logoUrl"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "logoUrl")
          }
        }

        public var websiteUrl: String {
          get {
            return resultMap["websiteUrl"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "websiteUrl")
          }
        }
      }

      public struct Tag: GraphQLSelectionSet {
        public static let possibleTypes = ["Tag"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String) {
          self.init(unsafeResultMap: ["__typename": "Tag", "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}

public final class PostJobMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition =
    """
    mutation postJob($title: String!, $companyName: String!, $locationName: String!, $userEmail: String!, $description: String!, $applyUrl: String!) {
      postJob(input: {title: $title, commitmentId: "cjtu8esth000z0824x00wtp1i", companyName: $companyName, locationNames: $locationName, userEmail: $userEmail, description: $description, applyUrl: $applyUrl}) {
        __typename
        id
        title
        userEmail
        applyUrl
        description
        locationNames
      }
    }
    """

  public let operationName = "postJob"

  public var title: String
  public var companyName: String
  public var locationName: String
  public var userEmail: String
  public var description: String
  public var applyUrl: String

  public init(title: String, companyName: String, locationName: String, userEmail: String, description: String, applyUrl: String) {
    self.title = title
    self.companyName = companyName
    self.locationName = locationName
    self.userEmail = userEmail
    self.description = description
    self.applyUrl = applyUrl
  }

  public var variables: GraphQLMap? {
    return ["title": title, "companyName": companyName, "locationName": locationName, "userEmail": userEmail, "description": description, "applyUrl": applyUrl]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes = ["Mutation"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("postJob", arguments: ["input": ["title": GraphQLVariable("title"), "commitmentId": "cjtu8esth000z0824x00wtp1i", "companyName": GraphQLVariable("companyName"), "locationNames": GraphQLVariable("locationName"), "userEmail": GraphQLVariable("userEmail"), "description": GraphQLVariable("description"), "applyUrl": GraphQLVariable("applyUrl")]], type: .nonNull(.object(PostJob.selections))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(postJob: PostJob) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "postJob": postJob.resultMap])
    }

    public var postJob: PostJob {
      get {
        return PostJob(unsafeResultMap: resultMap["postJob"]! as! ResultMap)
      }
      set {
        resultMap.updateValue(newValue.resultMap, forKey: "postJob")
      }
    }

    public struct PostJob: GraphQLSelectionSet {
      public static let possibleTypes = ["Job"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("userEmail", type: .scalar(String.self)),
        GraphQLField("applyUrl", type: .scalar(String.self)),
        GraphQLField("description", type: .scalar(String.self)),
        GraphQLField("locationNames", type: .scalar(String.self)),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, title: String, userEmail: String? = nil, applyUrl: String? = nil, description: String? = nil, locationNames: String? = nil) {
        self.init(unsafeResultMap: ["__typename": "Job", "id": id, "title": title, "userEmail": userEmail, "applyUrl": applyUrl, "description": description, "locationNames": locationNames])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      public var title: String {
        get {
          return resultMap["title"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      public var userEmail: String? {
        get {
          return resultMap["userEmail"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "userEmail")
        }
      }

      public var applyUrl: String? {
        get {
          return resultMap["applyUrl"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "applyUrl")
        }
      }

      public var description: String? {
        get {
          return resultMap["description"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "description")
        }
      }

      public var locationNames: String? {
        get {
          return resultMap["locationNames"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "locationNames")
        }
      }
    }
  }
}
