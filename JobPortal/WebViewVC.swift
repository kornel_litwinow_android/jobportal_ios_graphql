//
//  WebViewVC.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 19/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {
    var url = ""
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

         let url = URL(string: self.url)!
        let request = URLRequest(url: url)
        webView.load(request)
    }
    



}
