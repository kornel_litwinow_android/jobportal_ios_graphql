//
//  PostJobVC.swift
//  JobPortal
//
//  Created by Kornel Litwinow on 19/02/2020.
//  Copyright © 2020 Kornel Litwinow. All rights reserved.
//

import UIKit
import Apollo

class PostJobVC: UIViewController {
    
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var companyTxt: UITextField!
    @IBOutlet weak var locationTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var descriptionTxt: UITextField!
    @IBOutlet weak var urlTxt: UITextField!
    @IBOutlet weak var postJobBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postJobBtn.layer.cornerRadius = 20.0
     
    }
    
    private func showErrorAlert(title: String, message: String) {
         let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
         alert.addAction(UIAlertAction(title: "OK", style: .default))
         self.present(alert, animated: true)
       }
    

    @IBAction func postJob(_ sender: Any) {
        
        let desc = descriptionTxt.text
        let title = titleTxt.text
        let url = urlTxt.text
        let location = locationTxt.text
        let company = companyTxt.text
        let email = emailTxt.text
        
        Network.shared.apollo.perform(mutation: PostJobMutation(title: title!, companyName: company!, locationName: location!, userEmail: email!, description: desc!, applyUrl: url!)) { [weak self] result in
          guard let self = self else {
            return
          }
          switch result {
          case .success(let graphQLResult):
            if let jobPostResult = graphQLResult.data?.postJob {
                self.showErrorAlert(title: "Post Job", message: "Offer was added")
                print(jobPostResult)
            }

            if let errors = graphQLResult.errors {
                self.showErrorAlert(title: errors.description, message: "errors")
            }
          case .failure(let error):
            self.showErrorAlert(title: "Network Error", message: error.localizedDescription)
          }
        }
      }
    }
